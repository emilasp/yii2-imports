<?php
namespace emilasp\imports\parser\controllers;

use emilasp\imports\parser\parsers\MaxRunParser;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Class ParserController
 * @package emilasp\imports\backend\controllers
 */
class ParserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback
            ],
        ];
    }


    public function actionIndex()
    {
        $parsers = [
            'MaxRunParser' => '\emilasp\imports\parser\parsers\MaxRunParser'
        ];
        
        if ($parserName = Yii::$app->request->get('parser')) {
            return $this->render('parse', ['parser' => $parsers[$parserName]]);
        }

        /*$parser = new MaxRunParser();

        $parser->run();*/
        
        return $this->render('index', ['parsers' => $parsers]);
    }
    
    
}
