<?php
namespace emilasp\imports\parser\base;

use Symfony\Component\DomCrawler\Crawler;
use yii;
use yii\base\Component;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 23.03.16
 * Time: 20:26
 */
abstract class BaseParser extends Component
{
    const PARSER_TYPE_CRAWLER = 'crawler';
    const PARSER_TYPE_REGEXP  = 'regexp';

    const RULE_TYPE_VIRTUAL   = 'virtual';
    const RULE_TYPE_TEXT      = 'text';
    const RULE_TYPE_HTML      = 'html';
    const RULE_TYPE_ATTRIBUTE = 'attribute';

    const TYPE_LINKS = 'links';
    const TYPE_ITEM  = 'item';

    const METHOD_PREFIX = 'attr';


    public $baseUrl;
    public $curentUrl;

    protected $data      = [];
    protected $itemLinks = [];


    /** Получаем конфиг парсера
     * @return array
     */
    abstract public function getIterationConfig();

    public function run()
    {
        $iterationConfig = $this->getIterationConfig();

        foreach ($iterationConfig as $conf) {
            $this->data[] = $this->parseRecursive($conf);
        }
        $this->itemLinks = array_unique($this->itemLinks);
    }

    public function runOnce($configure)
    {
        if (strlen($configure['url']) < 3) {
            return ['class' => 'parser', 'method' => 'setResult', 'data' => ['Empty url..']];
        }

        $level = $configure['recurse'];
        $config = $this->getIterationConfig();

        $curConf = $config[0];
        for ($i = 0; $i <= $level; $i++) {
            if ($i !== $level) {
                $curConf = $curConf['sub'];
            }
        }

        if (isset($curConf['sub'])) {
            unset($curConf['sub']);
        }
        unset($configure['rule']);

        $curConf = ArrayHelper::merge($curConf, $configure);

        return ['class' => 'parser', 'method' => 'setResult', 'data' => $this->parseRecursive($curConf)];
    }


    /** Парсим по конфигу рекурсивно
     *
     * @param $config
     *
     * @return array
     */
    private function parseRecursive(array $config) : array
    {
        $data            = [];
        $this->curentUrl = $config['url'];
        $html            = $this->getContent($config['url']);

        if ($config['type'] === self::TYPE_LINKS) {
            $links = $this->getLinks($html, $config);

            /** Branches pager parse  */
            if (isset($config['pager']['rule'])) {
                $configPagerLinks         = $config;
                $configPagerLinks['rule'] = $config['pager']['rule'];
                unset($config['pager']);
                $pagerLinks = $this->getLinks($html, $configPagerLinks);
                if ($pagerLinks) {
                    $configPager = $config;
                    foreach ($pagerLinks as $pagerLink) {
                        $configPager['url'] = $pagerLink;
                        $data               = $data + $this->parseRecursive($configPager);
                    }
                }
            }
            /** END Branchs pager parse  */

            foreach ($links as $link) {
                if (isset($config['sub'])) {
                    $config['sub']['url'] = $link;
                    $data[$link]          = $this->parseRecursive($config['sub']);
                } else {
                    $data[] = $link;
                }
            }

            //$data = array_unique($data);

        } elseif ($config['type'] === self::TYPE_ITEM) {
            $data = $this->getDataFromContent($html, $config);
        }
        return $data;
    }


    /** Получаем контент
     *
     * @param $url
     *
     * @return mixed
     */
    private function getContent(string $url) : string
    {
        $ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';

        $timeout = 5;
        $ch      = curl_init($url);
        curl_setopt($ch, CURLOPT_USERAGENT, $ua);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $html = curl_exec($ch);
        curl_close($ch);

        return $html;
    }

    /** Получаем ссылки из dom
     *
     * @param $html
     * @param $config
     *
     * @return Crawler
     */
    private function getLinks(string $html, array $config) : string
    {
        if ($config['parserType'] === self::PARSER_TYPE_CRAWLER) {
            $crawler = new Crawler();
            $crawler->addHTMLContent($html, Yii::$app->charset);

            return $crawler->filter($config['rule'])->each(
                function (Crawler $node, $i) use ($config) {
                    $link = $node->attr('href');
                    if (isset($config['exclude'])) {
                        foreach ($config['exclude'] as $exclude) {
                            if (strpos($link, $exclude) !== false) {
                                $link = '';
                            }
                        }
                    }
                    return $link;
                }
            );
        }
    }

    /** Собираем и сохраняем данные
     * @param $html
     * @param $config
     *
     * @return array
     * @throws \Exception
     */
    private function getDataFromContent(string $html, array $config) : array
    {
        $data = [];
        if ($config['parserType'] === self::PARSER_TYPE_CRAWLER) {
            $crawler = new Crawler();
            $crawler->addHTMLContent($html, Yii::$app->charset);

            foreach ($config['rule'] as $attribute => $ruleOptions) {
                $rule     = $ruleOptions['rule'];
                $ruleType = $ruleOptions['ruleType'];

                if ($ruleType === self::RULE_TYPE_VIRTUAL) {
                    $data[$attribute] = [];
                    continue;
                }

                $nodes = $crawler->filter($rule);

                if (isset($ruleOptions['ruleMethod'])) {
                    $nodes = $nodes->{$ruleOptions['ruleMethod']}();
                }

                if ($ruleType === self::RULE_TYPE_TEXT) {
                    $data[$attribute] = $nodes->each(function (Crawler $node, $i) use ($ruleOptions) {
                        return $this->getValue($node->text(), $ruleOptions);
                    });
                } elseif ($ruleType === self::RULE_TYPE_HTML) {

                    $data[$attribute] = $nodes->each(function (Crawler $node, $i) use ($ruleOptions) {
                        return $this->getValue($node->html(), $ruleOptions);
                    });
                } elseif ($ruleType === self::RULE_TYPE_ATTRIBUTE) {
                    $data[$attribute] = $nodes->each(function (Crawler $node, $i) use ($ruleOptions) {
                        return $this->getValue($node->attr($ruleOptions['attribute']), $ruleOptions);
                    });
                }

                $data[$attribute] = array_values(array_diff($data[$attribute], [null]));

                if (!count($data[$attribute])) {
                    if (isset($ruleOptions['default'])) {
                        $data[$attribute] = $ruleOptions['default'];
                    }
                }
            }
            $data = $this->normalizeData($data);

            $this->saveItem($data);

            return $data;
        }
    }

    /**
     * Сохраняем данные
     */
    /**
     * @param array $data
     *
     * @throws \Exception
     * @throws yii\db\Exception
     */
    private function saveItem(array &$data)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $data = $this->afterParseItem($data);
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /** Обрабатываем и фильтруем значения
     *
     * @param $value
     * @param $config
     *
     * @return mixed
     */
    private function getValue(string $value, array $config) : string
    {
        if (isset($config['regexp'])) {
            $value = $this->getValueByRegexp($value, $config);
        }
        if (isset($config['callable']) && is_callable($config['callable'])) {
            $funct = $config['callable'];
            $value = $funct($value);
        }
        $value = trim(str_replace(["\n", "\r", "\t", '&nbsp;', ' ', "\x20"], ' ', $value));
        return $value;
    }

    /** Фильтруем по регулярному выражению
     *
     * @param $value
     * @param $config
     *
     * @return mixed
     */
    private function getValueByRegexp(string $value, array $config)
    {

        // $value = preg_quote($value);

        preg_match($config['regexp'], $value, $matches);
        $cntMatches = count($matches);
        if ($cntMatches) {
            return trim($matches[$cntMatches - 1]);
        }
        return null;
    }


    /** Обрабатываем атрибуты пользовательскими методами
     *
     * @param $data
     *
     * @return mixed
     */
    protected function normalizeData(array $data) : array
    {
        foreach ($data as $attribute => $value) {
            $method = self::METHOD_PREFIX . ucfirst($attribute);

            if (method_exists(static::className(), $method)) {
                static::$method($attribute, $data);
            }
        }
        return $data;
    }

    /**
     * Вызывается после Парсинга и обработки ITEM
     */
    public function afterParseItem(array $data) : array
    {
        return $data;
    }
}