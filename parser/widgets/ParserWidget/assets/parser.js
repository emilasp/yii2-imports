Parser = function () {
    var obj = this;
    
    this.init = function () {
        obj.blockWsClient = $('#ws-client');

        $('.btn-start').on('click', function () {
            var button = $(this);
            var level = button.data('level');
            var block = button.closest('.parser-cont');
            var data = block.data();
            data['parserType'] = block.data('parsertype');
            if (typeof data['pagers'] !== 'undefined') {
                data['pager'] = data['pagers'];
                console.log('pager rule');
            }
            console.log(data);
            var blockResultPrev = $('.result-' + (level - 1));
            var blockResult = $('.result-' + level);

            $('.block-result').removeClass('active');

                var urls = blockResultPrev.find('div.url').not('.final');

                if (urls.length) {
                    var urlBloc = $(urls[0]);
                    data['url'] = urlBloc.text();
                    urlBloc.addClass('final');
                } else {
                    return;
                }

            blockResult.addClass('active');
            websocket.send('handler', data);
        });
    };
    this.setResult = function (data) {
        var blockResult = $('.block-result.active');
        var mainCont = blockResult.closest('.parser-cont');
        var blockProgress = mainCont.find('.progress .progress-bar');
        var button = mainCont.find('.btn-start');
        var level = button.data('level');

        var blockResultPrev = $('.result-' + (level - 1));

        var countAllLinks = blockResultPrev.find('.url').length;
        var countFinalLinks = blockResultPrev.find('.url.final').length;

        var percent = parseInt(countFinalLinks/(countAllLinks/100)).toString() + '%';

        blockProgress.css('width', percent);
        blockProgress.text(percent);

        if (blockResult.hasClass('is-item')) {
            var html = '<hr />';
            $.each(data, function (index, item) {
                html += '<div class="row"><div class="col-md-3">'+index+'</div><div class="col-md-9">' + item + '</div></div>';
            });
            blockResult.append(html);
        } else {
            $.each(data, function (index, item) {
                blockResult.append('<div class="url">' + item + '</div>');
            });
        }


        var countParseLinks = blockResult.find('.url').length;

        mainCont.find('.result-monitor').html(countFinalLinks + '/' + countAllLinks + ' (' + countParseLinks + ')');
        button.click();
    }
};

var parser;
$(function () {
    parser = new Parser();
    parser.init();
});
