<?php

namespace emilasp\imports\parser\widgets\ParserWidget;

use emilasp\core\components\base\Widget;
use emilasp\core\helpers\CryptHelper;
use emilasp\imports\parser\base\BaseParser;
use emilasp\websocket\common\components\WsConnect;
use yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class ParserWidget
 * @package emilasp\imports\parser\widgets\ParserWidget
 */
class ParserWidget extends Widget
{
    /** @var  BaseParser */
    public $parser;
    public $parserClass;

    private $config;

    public function init()
    {
        $this->parserClass = $this->parser;
        $this->parser      = new $this->parser();
        $this->config      = $this->parser->getIterationConfig();
        $this->registerAssets();
    }

    public function run()
    {
        echo $this->getHtml();
    }

    private function getHtml()
    {
        $html = '';
        $html .= Html::beginTag('div', ['class' => 'row']);
        $html .= Html::beginTag('div', ['class' => 'col-md-6']);


        foreach ($this->config as $conf) {
            $html .= Html::tag(
                'div',
                Html::tag('div', $conf['url'], ['class' => 'url']),
                ['class' => 'block-result block-result-small result-0']
            );

            $html .= $this->getHtmlRecursive($conf);
        }

        $html .= Html::endTag('div');
        $html .= Html::beginTag('div', ['class' => 'col-md-6']);

        $html .= Html::tag('div', 'Result', ['class' => 'result']);

        $html .= Html::endTag('div');
        $html .= Html::endTag('div');

        return $html;
    }


    private function getHtmlRecursive($conf, $level = 1)
    {
        if ($conf['type'] === BaseParser::TYPE_ITEM) {
            $conf['rule'] = json_encode($conf['rule']);
        }
        
        $optionsMainContainer = [
            'id'              => 'container-' . $level,
            'class'           => 'parser-cont margin-' . $level,
            'data-class'      => $this->parserClass,
            'data-method'     => 'runOnce',
            'data-type'       => $conf['type'],
            'data-parserType' => $conf['parserType'],
            'data-rule'       => $conf['rule'],
            'data-recurse'    => $level - 1,
        ];

        if (isset($conf['exclude'])) {
            $optionsMainContainer['data-exclude'] = $conf['exclude'];
        }
        if (isset($conf['pager'])) {
            $optionsMainContainer['data-pagers'] = $conf['pager'];
        }
        $html = Html::beginTag('div', $optionsMainContainer);
        $html .= Html::button('Start', ['class' => 'btn btn-success btn-start', 'data-level' => $level]);
        $html .= Html::beginTag('div', ['class' => 'row']);
        $html .= Html::beginTag('div', ['class' => 'col-md-3']);
        $html .= Html::tag('h3', $conf['label']);
        $html .= Html::tag('h4', substr($conf['rule'], 0, 20));
        $html .= Html::tag('div', '', ['class' => 'result-monitor result-monitor-' . $level]);

        $html .= '  <div class="progress progress-"' . $level . '>
                      <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                    </div>';



        $html .= Html::endTag('div');
        $html .= Html::beginTag('div', ['class' => 'col-md-9']);

        $html .= Html::beginTag('div', [
            'class' => 'block-result result-' . $level . ' ' .($conf['type'] === BaseParser::TYPE_ITEM ? 'is-item' : '')
        ]);

        $html .= 'Result';

        $html .= Html::endTag('div');
        $html .= Html::endTag('div');
        $html .= Html::endTag('div');

        $html .= Html::endTag('div');

        if (isset($conf['sub'])) {
            $html .= $this->getHtmlRecursive($conf['sub'], $level + 1);
        }

        return $html;
    }


    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        ParserWidgetAsset::register($view);
    }
}
