<?php
namespace emilasp\imports\parser\widgets\ParserWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class WsAsset
 * @package emilasp\imports\parser\widgets\ParserWidget
 */
class ParserWidgetAsset extends AssetBundle
{
    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $js = ['parser'];
    public $css = ['parser'];
    public $sourcePath = __DIR__ . '/assets';
}
