<?php
namespace emilasp\imports\parser;

use emilasp\core\CoreModule;
use Yii;
use yii\base\BootstrapInterface;
use yii\console\Application;
use yii\helpers\FileHelper;

/**
 * Class ParserModule
 * i */
class ParserModule extends \emilasp\imports\ImportsModule
{
    /** @var string */
    public $id        = 'imports';
    public $pathCache = '@console/runtime/import/cache';
    public $pathIn    = '@console/runtime/import/parse/in';
    public $pathOut   = '@console/runtime/import/parse/out';

    public $imports = [];

    

}
