<?php
namespace emilasp\imports\parser\parsers;

use emilasp\taxonomy\models\Category;
use emilasp\im\common\models\Brand;
use emilasp\im\common\models\Product;
use emilasp\imports\parser\base\BaseParser;
use emilasp\taxonomy\models\Property;
use emilasp\taxonomy\models\PropertyGroup;
use emilasp\taxonomy\models\PropertyLink;
use emilasp\taxonomy\models\PropertyValue;
use \Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class MaxRunParser
 * @package emilasp\imports\backend\parsers
 */
class MaxRunParser extends BaseParser
{
    public $baseUrl = 'http://maxrun.ru';

    /** Основные настройки парсера по сбору ссылок и парсинга контента
     * @return array
     */
    public function getIterationConfig() : array
    {
        return [
            [
                'label'      => 'Url категорий',
                'url'        => 'http://maxrun.ru/products',
                'type'       => self::TYPE_LINKS,
                'parserType' => self::PARSER_TYPE_CRAWLER,
                'rule'       => '.category-capt-txt a',
                'exclude'    => ['products/category/1761315'],

                'sub' => [
                    'label'      => 'Url товаров',
                    'type'       => self::TYPE_LINKS,
                    'parserType' => self::PARSER_TYPE_CRAWLER,
                    'rule'       => '.product-link a',

                    'pager' => [
                        'rule' => '.pagination a',
                    ],

                    'sub' => [
                        'label'      => 'Продукты',
                        //'url'        => 'http://maxrun.ru/products/ecco-yak-muzhskie-temno-sinie-s-meham',
                        'type'       => self::TYPE_ITEM,
                        'parserType' => self::PARSER_TYPE_CRAWLER,
                        'rule'       => [
                            'name'      => [
                                'rule'     => 'header h1',
                                'ruleType' => self::RULE_TYPE_TEXT,
                            ],
                            'article'   => [
                                'rule'     => '.product-serial',
                                'regexp'   => '/Артикул:[\W]+([\w-]+)/',
                                'ruleType' => self::RULE_TYPE_TEXT,
                            ],
                            'cost_base' => [
                                'rule'       => '.old-price .product-price-data',
                                'ruleType'   => self::RULE_TYPE_TEXT,
                                'ruleMethod' => 'first',
                            ],
                            'cost'      => [
                                'rule'       => '.sale-price .product-price-data',
                                'ruleType'   => self::RULE_TYPE_TEXT,
                                'ruleMethod' => 'first',
                            ],
                            'size'      => [
                                'rule'     => '#product-params-view .ptype-view-select option',
                                'ruleType' => self::RULE_TYPE_TEXT,
                                'default'  => [
                                    '36 EUR', '37 EUR', '38 EUR', '39 EUR', '40 EUR', '41 EUR', '42 EUR',
                                ],
                            ],
                            'sex'       => [
                                'rule'     => '.user-inner',
                                'callable' => function ($value) {
                                    $curIndex   = 0;
                                    $arrPrepare = ['Модель', 'Цвет', 'Материал', 'Вес', 'Бренд',];
                                    $curEl      = ArrayHelper::remove($arrPrepare, $curIndex);
                                    $arrValues  = preg_split('/' . implode('|', $arrPrepare) . '/', $value);
                                    foreach ($arrValues as $value) {
                                        if (mb_strrpos($value, $curEl) !== false) {
                                            $tmp = explode($curEl, $value);
                                            return trim($tmp[1], ' :');
                                        }
                                    }
                                },
                                'ruleType' => self::RULE_TYPE_TEXT,
                            ],
                            'color'     => [
                                'rule'     => '.user-inner',
                                'callable' => function ($value) {
                                    $curIndex   = 1;
                                    $arrPrepare = ['Модель', 'Цвет', 'Материал', 'Вес', 'Бренд',];
                                    $curEl      = ArrayHelper::remove($arrPrepare, $curIndex);
                                    $arrValues  = preg_split('/' . implode('|', $arrPrepare) . '/', $value);
                                    foreach ($arrValues as $value) {
                                        if (mb_strrpos($value, $curEl) !== false) {
                                            $tmp = explode($curEl, $value);
                                            return trim($tmp[1], ' :');
                                        }
                                    }
                                },
                                'ruleType' => self::RULE_TYPE_TEXT,
                            ],
                            'material'  => [
                                'rule'     => '.user-inner',
                                'callable' => function ($value) {
                                    $curIndex   = 2;
                                    $arrPrepare = ['Модель', 'Цвет', 'Материал', 'Вес', 'Бренд',];
                                    $curEl      = ArrayHelper::remove($arrPrepare, $curIndex);
                                    $arrValues  = preg_split('/' . implode('|', $arrPrepare) . '/', $value);
                                    foreach ($arrValues as $value) {
                                        if (mb_strrpos($value, $curEl) !== false) {
                                            $tmp = explode($curEl, $value);
                                            return trim($tmp[1], ' :');
                                        }
                                    }
                                },
                                'ruleType' => self::RULE_TYPE_TEXT,
                            ],
                            'weight'    => [
                                'rule'     => '.user-inner',
                                'callable' => function ($value) {
                                    $curIndex   = 3;
                                    $arrPrepare = ['Модель', 'Цвет', 'Материал', 'Вес', 'Бренд',];
                                    $curEl      = ArrayHelper::remove($arrPrepare, $curIndex);
                                    $arrValues  = preg_split('/' . implode('|', $arrPrepare) . '/', $value);
                                    foreach ($arrValues as $value) {
                                        if (mb_strrpos($value, $curEl) !== false) {
                                            $tmp = explode($curEl, $value);
                                            return trim($tmp[1], ' :');
                                        }
                                    }
                                },
                                'ruleType' => self::RULE_TYPE_TEXT,
                            ],
                            'brand'     => [
                                'rule'     => '.user-inner',
                                'callable' => function ($value) {
                                    $curIndex   = 4;
                                    $arrPrepare = ['Модель', 'Цвет', 'Материал', 'Вес', 'Бренд',];
                                    $curEl      = ArrayHelper::remove($arrPrepare, $curIndex);
                                    $arrValues  = preg_split('/' . implode('|', $arrPrepare) . '/', $value);
                                    foreach ($arrValues as $value) {
                                        if (mb_strrpos($value, $curEl) !== false) {
                                            $tmp = explode($curEl, $value);
                                            return trim($tmp[1], ' :');
                                        }
                                    }
                                },
                                'ruleType' => self::RULE_TYPE_TEXT,
                            ],

                            'text' => [
                                'rule'       => '.user-inner',
                                //'ruleMethod' => 'last',
                                //'regexp'   => '/О модели:(*.)/',
                                'ruleType'   => self::RULE_TYPE_TEXT,
                            ],

                            'model'  => [
                                'rule'       => '.section-bread-crumbs a',
                                'ruleMethod' => 'last',
                                'ruleType'   => self::RULE_TYPE_TEXT,
                            ],
                            'images' => [
                                'rule'      => '.avatar-wrap a.fancy-img',
                                'attribute' => 'href',
                                'ruleType'  => self::RULE_TYPE_ATTRIBUTE,
                            ],

                            'category' => [
                                'rule'     => '',
                                'ruleType' => self::RULE_TYPE_VIRTUAL,
                            ],
                            'season'   => [
                                'rule'     => '',
                                'ruleType' => self::RULE_TYPE_VIRTUAL,
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    public function run()
    {
        parent::run();
        foreach ($this->itemLinks as $index => $link) {
            echo $index . ') ' . $link . '<br />';
        }
    }

    /** Обрабатываем наименование
     *
     * @param $attribute
     * @param $data
     */
    public function attrName(string $attribute, array &$data)
    {
        $value = $data[$attribute][0];

        if (strpos($value, '(')) {
            $arrName = explode('(', $value);
            $value   = $arrName[0];
        }

        $data[$attribute] = [trim($value)];
    }

    /** Обрабатываем описание
     *
     * @param $attribute
     * @param $data
     */
    public function attrText(string $attribute, array &$data)
    {
        if (isset($data[$attribute][0])) {
            $value = implode(',', $data[$attribute]);
        } else {
            $value = '';
        }

        if (mb_strpos($value, 'О модели')) {
            $arrName = explode('О модели', $value);
            $value   = trim($arrName[1], ': ');

            if (mb_strpos($value, 'Определить размер')) {
                $arrName = explode('Определить размер', $value);
                $value   = $arrName[0];
            }
        } else {
            $value = '';
        }

        $data[$attribute] = [trim($value)];
    }

    /** Формируем цену
     *
     * @param $attribute
     * @param $data
     */
    public function attrCost(string $attribute, array &$data)
    {
        $data[$attribute] = [$data[$attribute][0] - random_int(3, 5) * 10];
    }

    /** Формируем базовую цену
     *
     * @param $attribute
     * @param $data
     */
    public function attrCost_base(string $attribute, array &$data)
    {
        $data[$attribute] = [$data[$attribute][0] - random_int(0, 10) * 10];
    }

    /** Обрабатываем размеры
     *
     * @param $attribute
     * @param &$data
     */
    public function attrSize(string $attribute, array &$data)
    {
        $value = array_diff($data[$attribute], ['-']);

        array_walk($value, function (&$item, $key) {
            $itemTmp = explode('|', $item);
            if (isset($itemTmp[1])) {
                $item = str_replace('EUR', '', $itemTmp[1]);
            }
        });

        $data[$attribute] = $value;
    }

    /** Шифруем артикул
     *
     * @param $attribute
     * @param &$data
     *
     * @return array
     */
    public function attrArticle(string $attribute, array &$data)
    {
        $value = str_split($data[$attribute][0]);
        foreach ($value as $index => $symbol) {
            $value[$index] = chr(ord($symbol) + 1);
        }
        $data[$attribute] = [implode('', $value)];
    }

    /** Шифруем артикул
     *
     * @param $attribute
     * @param &$data
     *
     * @return array
     */
    public function attrSex($attribute, &$data)
    {
        $valueAtrtibute = mb_strtolower($data[$attribute][0]);

        $male   = strpos($valueAtrtibute, 'муж') !== false;
        $famale = strpos($valueAtrtibute, 'жен') !== false;

        $value = 3;
        if ($male && !$famale) {
            $value = 1;
        } elseif (!$male && $famale) {
            $value = 2;
        }

        if (!$male && !$famale) {
            $valueAtrtibute = $data['name'][0];
            $male   = strpos($valueAtrtibute, 'муж') !== false;
            $famale = strpos($valueAtrtibute, 'жен') !== false;

            if ($male && !$famale) {
                $value = 1;
            } elseif (!$male && $famale) {
                $value = 2;
            }
        }
        $data[$attribute] = [$value];
    }

    /** Цвет
     *
     * @param $attribute
     * @param $data
     */
    public function attrColor(string $attribute, array &$data)
    {
        $value = $data[$attribute][0];
        $value = preg_replace("/\\(.+\\)/m", '', $value);
        $value = explode(',', $value);

        array_walk($value, function (&$item1, $key) {
            $item1 = trim(str_replace('&nbsp;', '', $item1));
        });

        $data[$attribute] = $value;
    }

    /** Вес
     *
     * @param $attribute
     * @param $data
     */
    public function attrWeight(string $attribute, array &$data)
    {
        $value = null;
        if (isset($data[$attribute][0])) {
            $value = $data[$attribute][0];
            $value = preg_replace('/[^0-9]/', '', $value);
        }

        $data[$attribute] = [$value];
    }

    public function attrSeason($attribute, &$data)
    {
        $patterns     = [
            'зимняя'   => 'Зима',
            'летняя'   => 'Лето',
            'зимн'     => 'Зима',
            'летн'     => 'Лето',
            'лето'     => 'Лето',
            'мехо'     => 'Зима',
            'эспадр'   => 'Лето',
            'босонож'  => 'Лето',
            'бутс'     => 'Лето',
            'кеды'     => 'Лето',
            'кроссовк' => 'Демисезон',
        ];
        $seasonResult = null;

        foreach ($patterns as $pattern => $season) {
            if (mb_stripos($data['name'][0], $pattern) !== false) {
                $seasonResult = $season;
                break;
            }
        }

        if (!$seasonResult) {
            foreach ($patterns as $pattern => $season) {
                if (mb_stripos($data['text'][0], $pattern) !== false) {
                    $seasonResult = $season;
                    break;
                }
            }
        }

        if ($seasonResult) {
            $data['season'] = [$seasonResult];
        }
    }

    public function attrMaterial($attribute, &$data)
    {
        $materials = $data[$attribute][0];
        $materials = explode(';', $materials);

        foreach ($materials as $materialPosition) {
            $mats = explode(',', str_replace(['верх -', 'внутри -', 'подошва -'], '', $materialPosition));

            if (mb_stripos($materialPosition, 'верх') !== false) {
                $data['materialTop'] = $mats;
            }
            if (mb_stripos($materialPosition, 'внутри') !== false) {
                $data['materialInner'] = $mats;
            }
            if (mb_stripos($materialPosition, 'подошва') !== false) {
                $data['materialSole'] = $mats;
            }
        }
    }

    /** Формируем порядок категорий
     *
     * @param $attribute
     * @param $data
     */
    public function attrCategory($attribute, &$data)
    {
        $patterns = [
            'кроссовк' => 'Кроссовки',
            'кроссово' => 'Кроссовки',
            'ботинк'   => 'Ботинки',
            'кеды'     => 'Кеды',
            'сапож'    => 'Сапоги',
            'сапог'    => 'Сапоги',
            'балетк'   => 'Балетки',
            'эспадр'   => 'Эспадрильи',
            'босонож'  => 'Босоножки',
            'бутс'     => 'Бутсы',
            'носки'    => 'Носки',
        ];

        $categories = [];

        if ($data['sex'][0] === 1) {
            $categories[0] = 'Мужская обувь';
        } elseif ($data['sex'][0] === 2) {
            $categories[0] = 'Женская обувь';
        } else {
            $categories[0] = ['Мужская обувь', 'Женская обувь'];
        }

        foreach ($patterns as $pattern => $cat) {
            if (mb_stripos($data['name'][0], $pattern) !== false) {
                $categories[1] = $cat;
                break;
            }
        }

        if (!isset($categories[1])) {
            foreach ($patterns as $pattern => $cat) {
                if (mb_stripos($data['text'][0], $pattern) !== false) {
                    $categories[1] = $cat;
                    break;
                }
            }
        }

        if (!isset($categories[1])) {
            $categories[1] = 'Кроссовки';
        }

        $data[$attribute] = $categories;
    }




    /** INSERT PRODUCTS
     */


    /**
     * @param $data
     *
     * @return mixed
     */
    public function afterParseItem($data)
    {
        //var_dump($data);
        //var_dump($this->curentUrl);

        $brand = $this->getBrand($data['brand'][0]);

        $product = Product::findOne(['article' => $data['article'][0]]);

        if ($product) {

        } else {
            $product            = new Product();
            $product->seller_id = 1;
            $product->brand_id  = $brand->id;
            $product->article   = $data['article'][0];
            $product->name      = $data['name'][0];
            $product->text      = $data['text'][0];
            $product->cost      = $data['cost'][0];
            $product->cost_base = $data['cost_base'][0];
            $product->discount  = (int)(100 - ($product->cost / ($product->cost_base / 100)));
            $product->unit      = 1;
            $product->weight    = $data['weight'][0];
            $product->sex       = $data['sex'][0];
            $product->type      = Product::TYPE_VARIATE_PSEUDO;
            $product->status    = 1;

            if ($product->save()) {
                $this->setCategory($product, $data['category']);
                $this->setProperty($product, 'size', $data['size']);
                $this->setProperty($product, 'color', $data['color']);

                $this->setProperty($product, 'model', $data['model']);

                if (isset($data['season'])) {
                    $this->setProperty($product, 'season', $data['season']);
                }

                if (isset($data['materialTop'])) {
                    $this->setProperty($product, 'materialTop', $data['materialTop']);
                }
                if (isset($data['materialInner'])) {
                    $this->setProperty($product, 'materialInner', $data['materialInner']);
                }
                if (isset($data['materialSole'])) {
                    $this->setProperty($product, 'materialSole', $data['materialSole']);
                }

                $this->addImages($product, $data['images']);
            }
        }
        return ['New product' => Html::a($product->name, '/')];
    }

    /** Получаем бренд
     *
     * @param $brandName
     *
     * @return Brand|null|static
     */
    private function getBrand($brandName)
    {
        $brandName = trim(str_replace('&nbsp;', '', $brandName));

        if (isset($this->commonData['brands'][$brandName])) {
            return $this->commonData['brands'][$brandName];
        }

        $this->commonData['brands'][$brandName] = Brand::findOne(['name' => $brandName]);
        if (!$this->commonData['brands'][$brandName]) {
            $this->commonData['brands'][$brandName]         = new Brand();
            $this->commonData['brands'][$brandName]->name   = $brandName;
            $this->commonData['brands'][$brandName]->status = 1;
            $this->commonData['brands'][$brandName]->save();
        }
        return $this->commonData['brands'][$brandName];
    }


    /** Загружаем изображения
     *
     * @param Product $product
     * @param array $images
     */
    private function addImages(Product $product, array $images)
    {
        foreach ($images as $image) {
            if (strpos($image, 'http:') === false) {
                $image = $this->baseUrl . $image;
            }
            $product->addImage($image);
        }
    }

    /** Устанавливаем свойства и их значения
     *
     * @param $product
     * @param $code
     * @param $values
     */
    private function setProperty($product, $code, $values)
    {
        $props = [
            'model'         => ['name' => 'Модель', 'type' => Property::TYPE_SELECT],
            'size'          => ['name' => 'Размер', 'type' => Property::TYPE_SELECT, 'is_variate' => 1],
            'color'         => ['name' => 'Цвет', 'type' => Property::TYPE_COLOR],
            'material'      => ['name' => 'Материал', 'type' => Property::TYPE_SELECT],
            'season'        => ['name' => 'Сезон', 'type' => Property::TYPE_SELECT],
            'materialTop'   => ['name' => 'Верх', 'type' => Property::TYPE_SELECT],
            'materialInner' => ['name' => 'Внутри', 'type' => Property::TYPE_SELECT],
            'materialSole'  => ['name' => 'Подошва', 'type' => Property::TYPE_SELECT],
        ];

        $propAttrs = $props[$code];

        if (!isset($this->commonData['properties'][$propAttrs['name']])) {
            $property = Property::findOne(['name' => $propAttrs['name']]);
            if (!$property) {
                if (in_array($code, ['materialTop', 'materialInner', 'materialSole'])) {
                    if (!isset($this->commonData['groups']['Материал'])) {
                        $group            = new PropertyGroup();
                        $group->name      = 'Материал';
                        $group->text      = 'Материал';
                        $group->visible   = 1;
                        $group->view_type = 1;
                        $group->order     = 10;
                        $group->status    = 1;
                        $group->save();
                        $this->commonData['groups']['Материал'] = $group;
                    }
                    $group = $this->commonData['groups']['Материал'];
                } else {
                    $group            = new PropertyGroup();
                    $group->name      = $propAttrs['name'];
                    $group->text      = $propAttrs['name'];
                    $group->visible   = 1;
                    $group->view_type = 1;
                    $group->order     = 10;
                    $group->status    = 1;
                    $group->save();
                }

                $property = new Property();
                $property->setAttributes($propAttrs);
                $property->filter_type_view = 1;
                $property->group_id         = $group->id;
                $property->status           = 1;
                $property->save();
            }
            $this->commonData['properties'][$propAttrs['name']] = $property;
        } else {
            $property = $this->commonData['properties'][$propAttrs['name']];
        }

        foreach ($values as $value) {
            //echo var_dump($value);
            $value = trim($value);
            if (!isset($this->commonData['values'][$property->id][$value])) {
                $valueModel = PropertyValue::findOne(['property_id' => $property->id, 'value' => $value]);

                if (!$valueModel) {
                    $valueModel              = new PropertyValue();
                    $valueModel->value       = $value;
                    $valueModel->property_id = $property->id;
                    $valueModel->data        = '';
                    $valueModel->save();
                }
                $this->commonData['values'][$property->id][$value] = $valueModel;
            } else {
                $valueModel = $this->commonData['values'][$property->id][$value];
            }

            PropertyLink::saveLink($property->id, $valueModel->id, $product->id, $product::className());
        }
    }

    /** Устанавлвиаем категории
     *
     * @param $product
     * @param $categories
     */
    private function setCategory($product, $categories)
    {
        $this->getBaseCategory();

        $categories = $this->getCategories($categories[1], $product->sex);

        foreach ($categories as $category) {
            $product->link('categories', $category, ['object' => $product::className()]);
        }

    }

    private function getCategories($categoryName, $sex)
    {
        if ($sex === 3) {
            if (!isset($this->commonData['categories'][1][$categoryName])) {
                $this->commonData['categories'][1][$categoryName] = $this->getCategory(
                    $this->commonData['categories'][1]['Мужская обувь'],
                    $categoryName,
                    1
                );
            }
            if (!isset($this->commonData['categories'][2][$categoryName])) {
                $this->commonData['categories'][2][$categoryName] = $this->getCategory(
                    $this->commonData['categories'][2]['Женская обувь'],
                    $categoryName,
                    2
                );
            }
            $result = [
                $this->commonData['categories'][1][$categoryName],
                $this->commonData['categories'][2][$categoryName],
            ];
        } else {
            if (!isset($this->commonData['categories'][$sex][$categoryName])) {
                $base                                                = $sex === 1 ? $this->commonData['categories'][1]['Мужская обувь'] : $this->commonData['categories'][2]['Женская обувь'];
                $this->commonData['categories'][$sex][$categoryName] = $this->getCategory(
                    $base,
                    $categoryName,
                    $sex
                );
            }
            $result = [
                $this->commonData['categories'][$sex][$categoryName],
            ];
        }
        return $result;
    }


    private function getBaseCategory()
    {
        if (!isset($this->commonData['categories']['base'])) {
            $this->commonData['categories']['base'] = Category::findOne(1);
        }

        if (!isset($this->commonData['categories'][1]['Мужская обувь'])) {
            $this->commonData['categories'][1]['Мужская обувь'] = $this->getCategory(
                $this->commonData['categories']['base'],
                'Мужская обувь',
                1
            );
        }

        if (!isset($this->commonData['categories'][2]['Женская обувь'])) {
            $this->commonData['categories'][2]['Женская обувь'] = $this->getCategory(
                $this->commonData['categories']['base'],
                'Женская обувь',
                2
            );
        }
    }

    private function getCategory($parentCategory, $categoryName, $sex)
    {
        $code     = $sex . '-' . $categoryName;
        $category = Category::findOne(['code' => $code]);

        if (!$category) {
            $category             = new Category();
            $category->code       = $code;
            $category->name       = $categoryName;
            $category->text       = $categoryName;
            $category->short_text = $categoryName;
            $category->title      = $categoryName;
            $category->status     = 1;
            $category->type       = 1;
            $category->status     = 1;
            $category->prependTo($parentCategory);
        }

        return $category;
    }
}


