<?php

/* @var $this yii\web\View */

use emilasp\websocket\common\widgets\ws\WsConsoleWidget;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title                   = Yii::t('im', 'Parsers');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= WsConsoleWidget::widget(['positionRight' => true]) ?>

<div class="site-index">
    <div class="body-content">
        
        <div class="parser-list">
            <?php foreach ($parsers as $index => $namespace) : ?>
                <?= Html::a($index, Url::toRoute(['/parser/parser/index', 'parser' => $index])) ?>
            <?php endforeach; ?>
        </div>
        
    </div>
</div>
