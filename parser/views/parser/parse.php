<?php

/* @var $this yii\web\View */

use emilasp\imports\parser\widgets\ParserWidget\ParserWidget;
use emilasp\websocket\common\widgets\ws\WsConsoleWidget;

$this->title                   = Yii::t('site', 'im');
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = $parser;
?>

<?= WsConsoleWidget::widget(['positionRight' => true]) ?>

<div class="site-index">
    <div class="body-content">
        
        <?= ParserWidget::widget(['parser' => $parser])?>
        
    </div>
</div>
