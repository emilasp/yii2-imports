<?php

namespace emilasp\imports\providers\exporters;

use emilasp\imports\base\Exporter;
use emilasp\im\common\models\Product;
use emilasp\taxonomy\models\Category;
use emilasp\taxonomy\models\Property;
use emilasp\taxonomy\models\PropertyLink;
use emilasp\taxonomy\models\PropertyValue;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;
use yii\log\Logger;

/**
 * Class ProductTableExporter
 * @package emilasp\imports\providers\exporters
 */
class ProductTableExporter extends Exporter
{
    /** Префикс для метода обработки колонки */
    const METHOD_PREFIX = 'col';

    /** @var array структура для вставки */
    public $columnsName = [
        'brand_id'   => null,
        'seller_id'  => null,
        'article'    => null,
        'name'       => null,
        'text'       => null,
        'short_text' => null,
        'cost_base'  => null,
        'cost'       => null,
        'quantity'   => null,
        'discount'   => null,
        'unit'       => null,
        'weight'     => null,
        'taxonomies' => null,
        'categories' => null,
        'images'     => null,
        'type'       => 1,
        'status'     => 1,
        'updated_at' => null,
        'created_by' => 1,
        'updated_by' => 1,
    ];

    /** @var integer ID агента */
    public $sellerId;

    /** Доформируем данные для вставки в БД
     *
     * @param $row
     *
     * @return array
     */
    public function formatRow($row)
    {
        $errors = [];

        $row = ArrayHelper::merge($this->columnsName, $row);

        $row['seller_id'] = $this->owner->sellerId;

        /*if (!$row['article']) {
            $this->owner->log('У товара не установлен артикул', Logger::LEVEL_WARNING);
        }*/

        if (!$row['article']) {
            $errors[] = 'У товара не установлен артикул';
        }

        if ($errors) {
            $this->owner->log(Json::encode($errors), Logger::LEVEL_ERROR);
            return null;
        }
        return $row;
    }

    /** Вставляем данные
     *
     * @param $data
     *
     * @return array
     */
    public function insertData($data)
    {
        $inserted = [];

        foreach ($data as $index => $row) {
            $product = Product::findOne(['seller_id' => $row['seller_id'], 'article' => $row['article']]);

            if ($product) {
                $this->updateProduct($product, $row);
            } else {
                $this->instertProduct($row);
            }
            /*if ($index > 10) {
                die;
            }*/
        }
        return $inserted;
    }

    /**
     * @param $data
     *
     * @return Product
     */
    private function instertProduct(array $data)
    {
        $taxonomies = $data['taxonomies'];
        $categories = $data['categories'];
        $images     = $data['images'];


        $product = new Product();
        $product->setAttributes($data);

        $product->validate();

        if (!$product->save() && $this->owner->isConsole) {
            Yii::$app->controller->message(json_encode($product->getErrors()) . "\n", Console::FG_GREEN);
        } else {
            $this->linkCategories($product, $categories);
            $this->addTaxonomies($product, $taxonomies);
            $this->addImages($product, $images);
            if ($this->owner->isConsole) {
                Yii::$app->controller->message("Inserted product {$data['article']}\n", Console::FG_GREEN);
            }
        }
        return $product;
    }

    /** Загружаем изображения
     *
     * @param Product $product
     * @param array $images
     */
    private function addImages(Product $product, array $images)
    {
        foreach ($images as $image) {
            $product->addImage($image);
        }
    }

    /** Привязываем категории
     *
     * @param Product $product
     * @param array $taxonomies
     */
    private function addTaxonomies(Product $product, array $taxonomies)
    {
        foreach ($taxonomies as $taxName => $taxanomy) {
            $property = Property::findOne(['name' => $taxName]);
            if (!$property) {
                $property                   = new Property();
                $property->name             = $taxName;
                $property->type             = 2;
                $property->filter_type_view = 1;
                $property->value_as_page    = 1;
                $property->group_id         = 1;
                $property->status           = 1;
                $property->save();
            }

            foreach ((array)$taxanomy as $value) {
                $valueModel = PropertyValue::findOne(['property_id' => $property->id, 'value' => $value]);

                if (!$valueModel) {
                    $valueModel              = new PropertyValue();
                    $valueModel->value       = $value;
                    $valueModel->property_id = $property->id;
                    $valueModel->data = '';
                    $valueModel->save();
                }

                PropertyLink::saveLink($property->id, $valueModel->id, $product->id, $product::className());
            }
        }
    }

    /** Привязываем категории
     *
     * @param Product $product
     * @param array $categories
     */
    private function linkCategories(Product $product, array $categories)
    {
        foreach ($categories as $cat) {
            $category = Category::findOne($cat);
            if ($category) {
                $product->link('categories', $category, ['object' => $product::className()]);
            }
        }
    }

    /**
     * @param Product $product
     * @param array $data
     *
     * @return Product
     */
    private function updateProduct(Product $product, array $data)
    {
        $product->setAttributes($data);
        if (!$product->save() && $this->owner->isConsole) {
            Yii::$app->controller->message(json_encode($product->getErrors()) . "\n", Console::FG_GREEN);
        } else {
            if ($this->owner->isConsole) {
                Yii::$app->controller->message("Updated product {$data['article']}\n", Console::FG_GREEN);
            }
        }
        return $product;
    }


    /**
     * @param Product $product
     * @param array $data
     *
     * @return Product
     */
    private function linkTaxonomies(Product $product, array $data)
    {
        return new Product();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function generateHash(array $data)
    {
        ksort($data);
        $dataWithoutTax = array_merge([], $data);
        unset($dataWithoutTax['taxonomies']);
        $hash = crc32(json_encode($dataWithoutTax));
        return $hash;
    }
}
