<?php

namespace emilasp\imports\controllers;

use emilasp\imports\base\ImportConf;
use emilasp\imports\controllers\base\BaseController;
use Yii;
use yii\helpers\Console;

/**
 * Class ImportSalesController
 * @package emilasp\imports\controllers\import
 */
class ImportController extends BaseController
{
    protected static $pidFile = 'worker_import_yii2';

    /** @var  ImportConf[] */
    private $confObjects = [];

    /** Импорт
     * @param $sellerId
     * @return int
     */
    public function actionImport($sellerId = null)
    {
        if ($sellerId) {
            $this->message('Поставщик: ' . $sellerId);
        } else {
            $this->message('Импорт по всем поставщикам');
        }

        $this->setConfObjects($sellerId);

        foreach ($this->confObjects as $conf) {
            $conf->process();
        }
    }

    /** Проверяем и устанавливаем конфигурацию
     * @param $sellerId
     * @return bool|null
     */
    private function setConfObjects($sellerId = null)
    {
        foreach ($this->module->imports as $conf) {
            $obj = Yii::createObject($conf);
            if ($sellerId === null || $obj->sellerId === (int)$sellerId) {
                $this->confObjects[] = $obj;
            }
        }

        if (count($this->confObjects) == 0) {
            if ($sellerId) {
                $this->message(
                    'Для поставщика с ID ' . $sellerId . ' не найдено класса конфигурации!',
                    Console::FG_RED
                );
            } else {
                $this->message('Не установлены классы конфигурации', Console::FG_RED);
            }
        }
    }
}
