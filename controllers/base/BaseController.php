<?php

namespace emilasp\imports\controllers\base;

use emilasp\core\helpers\FileHelper;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/** Базовый класс для планировщика задач
 * Class BaseController
 * @package emilasp\imports\controllers\base
 */
abstract class BaseController extends Controller
{
    protected static $pidFile = 'worker_sync_yii2';
    protected static $pathPid = '/run/';

    protected $time;
    protected $memory;

    public function init()
    {
        parent::init();

        echo PHP_EOL;
        /*if ($this->isActive()) {
            $this->printDateTime();
            $this->stdout("Скрипт уже выполяется..\n", Console::FG_RED);
            exit;
        }
        $this->lock();*/
        $this->printDateTime();
        $this->stdout("Стартуем воркер.\n", Console::FG_GREEN);
    }


    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->setProfiled();
        return parent::beforeAction($action);
    }

    /**
     * @param \yii\base\Action $action
     * @param mixed            $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        $this->getProfiled();
        return parent::afterAction($action, $result);
    }

    /** Выводим сообщение в консоль
     * @param     $message
     * @param int $color
     */
    public function message($message, $color = Console::FG_GREEN)
    {
        $this->printDateTime();
        $this->stdout($message . "\n", $color);
    }


    /**
     * Создаём файл блокировки
     */
    private function lock()
    {
        $this->printDateTime();
        $this->stdout("Лочим PID файл.\n", Console::FG_GREEN);
        file_put_contents(static::$pathPid . static::$pidFile, getmypid());
    }

    /** проверяем запущен ли процесс
     * @return bool
     */
    private function isActive()
    {
        if (!file_exists(static::$pathPid . static::$pidFile)) {
            $this->printDateTime();
            $this->stdout("Файл блокировки отсутствует..\n", Console::FG_GREEN);
            return false;
        }
        $pid = trim(file_get_contents(static::$pathPid . static::$pidFile));

        if ($pid && posix_kill($pid, 0)) {
            $this->printDateTime();
            $this->stdout("Воркер уже запущен.\n", Console::FG_RED);
            return true;
        } elseif (!empty($pid)) {
            $this->printDateTime();
            $this->stdout("Файл блокировки присутстует, но воркер нe запущен.\n", Console::FG_RED);
            if (!unlink(static::$pathPid . static::$pidFile)) {
                $this->printDateTime();
                $this->stdout("Воркер нe запущен, но не хватает прав для удаления PID файла\n", Console::FG_RED);
                exit(-1);
            }
        }
        return false;
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function getProfiled()
    {
        $this->stdout("---------------------------------\n", Console::FG_BLUE);
        $this->printDateTime();
        $this->stdout("Воркер успешно закончил работу\n", Console::FG_GREEN);
        $this->printDateTime();
        $this->stdout("Использовано памяти: ", Console::FG_BLUE);
        $this->stdout(FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . "\n", Console::FG_YELLOW);
        $this->printDateTime();
        $this->stdout("Время выполнения скрипта: ", Console::FG_BLUE);
        $this->stdout((microtime(true) - $this->time), Console::FG_YELLOW);
        $this->stdout(" сек.\n", Console::FG_BLUE);
    }

    /**
     * Устанавливаем начальные значения времени старта скрипта и используемой памяти
     */
    private function setProfiled()
    {
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
        $this->stdout("---------------------------------\n", Console::FG_BLUE);
    }

    /**
     * Выводим дату с временем в консоль
     */
    private function printDateTime()
    {
        $this->stdout("[", Console::FG_CYAN);
        $this->stdout(date('H:i:s'), Console::FG_PURPLE);
        $this->stdout("] ", Console::FG_CYAN);
    }
}
