Модуль Import для Yii2
=============================


Добавление нового импорта
------------

1. Добавляем новый компонент обработчик данных в common/modules/import/imports (см в папке)
2. Добавляем в common/configs/main.php параметры модуля для нового импорта

```php
'modules' => [
    'imports' => [
        'class' => \common\modules\imports\ImportsModule::className(),
        'imports' => [
            \common\modules\imports\imports\MdmConf::className()
        ]
    ],
],
```

3. Прописывам ID агента и тарифа в common/configs/params.php (для того что бы добавленные импортом записи в sales_import_buffer
 вставлялись в таблицу sales)

```php
'syncSales'=>[
    66  => [95],
],
```

4. Добавляем задание в крон
```bash
30 */12   * * *  root    /var/www/sale.dev/yii imports/import 66 > /dev/null 2>&1
```
