<?php
namespace emilasp\imports\helpers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use PHPExcel_IOFactory;

/** Вспомогательный класс для работы с файлами
 *
 * Class ImportHelper
 * @package emilasp\imports\helpers
 */
class ImportHelper extends FileHelper
{
    /** Дефолтная карта расширений */
    public static $extMapDefault = [
        'xml'  => 'xml',
        'xls'  => 'xls',
        'xlsx' => 'xlsx',
        'csv'  => 'csv',
        'doc'  => 'doc',
        'rtf'  => 'rtf',
    ];

    /** @var string алиас до временной папки */
    public static $tmpPath = '@console/runtime/tmp/';

    /** Получаем расширение файла
     * @param $filename
     * @return string
     */
    public static function getExt($filename)
    {
        return substr(strrchr($filename, '.'), 1);
    }

    /** Получаем список всех файлов в папке
     * @param $path
     * @param array $params
     * @return array
     */
    public static function getFileList($path, $params = [])
    {
        if (!is_dir($path)) {
            return [];
        }

        $is_full_path = !isset($params['fullPath']) || $params['fullPath'] == true;
        $is_all_files = empty($params['extensions']) || !is_array($params['extensions']);

        $file_list = [];
        $iterator  = new \DirectoryIterator($path);
        while ($iterator->valid()) {
            $file = $iterator->current();
            if ($file->isFile()) {
                if ($is_all_files) {
                    if ($is_full_path) {
                        $file_list[] = $file->getRealPath();
                    } else {
                        $file_list[] = $file->getBasename();
                    }
                } else {
                    $ext = strtolower($file->getExtension());
                    if (in_array($ext, $params['extensions'])) {
                        if ($is_full_path) {
                            $file_list[] = $file->getRealPath();
                        } else {
                            $file_list[] = $file->getBasename();
                        }
                    }
                }
            }
            $iterator->next();
        }
        return $file_list;
    }

    /** Получаем строки их разных форматов табличных файлов
     * @param       $file
     * @param array $extMap
     * @param array $params
     * @return array|mixed
     */
    public static function parse($file, $extMap = [], $params = [])
    {
        $extMap   = ArrayHelper::merge(self::$extMapDefault, $extMap);
        $file_ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

        $data = [[]];
        if (isset($extMap[$file_ext])) {
            switch ($extMap[$file_ext]) {
                case 'xls':
                    $data = self::parseXLS($file, $params);
                    break;
                case 'xlsx':
                    $data = self::parseXLSX($file, $params);
                    break;
                case 'xml':
                    $data = self::parseXML($file, $params);
                    break;
                case 'csv':
                    $data = self::parseCSV($file, $params);
                    break;
                case 'doc':
                    $data = self::parseDOC($file, $params);
                    break;
                case 'rtf':
                    $data = self::parseRTF($file, $params);
                    break;
            }
        }
        return $data;
    }

    /** Парсим xls
     * @param $file
     * @param $params
     * @return array
     */
    private static function parseXLS($file, $params = [])
    {
        $xls    = PHPExcel_IOFactory::load($file);
        $sheets = $xls->getAllSheets();

        $out = [];
        foreach ($sheets as $sheet) {
            $data = $sheet->toArray();
            $arr  = $data;
            foreach ($arr as $line_num => $arrLine) {
                foreach ($arrLine as $cell_num => $cell) {
                    if (empty($cell)) {
                        unset($data[$line_num][$cell_num]);
                    }
                }
                if (count($data[$line_num]) == 0) {
                    unset($data[$line_num]);
                }
            }
            $out[] = $data;
        }

        return $out;
    }

    /** Парсим xlsx
     * @param $file
     * @param $params
     * @return array
     */
    private static function parseXLSX($file, $params = [])
    {
        $extract_dir = Yii::getAlias(self::$tmpPath);
        $zip         = new \ZipArchive();
        $res         = $zip->open($file);
        if ($res === true) {
            $zip->extractTo($extract_dir);
            $zip->close();
        }
        $xml              = simplexml_load_file($extract_dir . '/xl/sharedStrings.xml');
        $sharedStringsArr = [];
        foreach ($xml->children() as $item) {
            $sharedStringsArr[] = (string)$item->t;
        }
        $handle = @opendir($extract_dir . '/xl/worksheets');
        $out    = [];

        while ($file = @readdir($handle)) {
            //проходим по всем файлам из директории /xl/worksheets/
            if ($file != "." && $file != ".." && $file != '_rels') {
                $xml       = simplexml_load_file($extract_dir . '/xl/worksheets/' . $file);
                $sheetData = [];
                //по каждой строке
                $row = 0;
                foreach ($xml->sheetData->row as $item) {
                    $sheetData[$row] = [];
                    //по каждой ячейке строки
                    $cell = 0;
                    foreach ($item as $child) {
                        $attr                   = $child->attributes();
                        $value                  = isset($child->v) ? (string)$child->v : false;
                        $sheetData[$row][$cell] = (isset($attr['t']) && $attr['t'] == 's')
                            ? $sharedStringsArr[$value] : $value;
                        $cell++;
                    }
                    $row++;
                }

                $out[] = $sheetData;
            }
        }
        return $out;
    }

    /** Парсим xml
     * @param $file
     * @param $params
     * @return array
     */
    private static function parseXML($file, $params = [])
    {
        $xml  = simplexml_load_string(file_get_contents($file), null, LIBXML_NOCDATA);
        $json = json_encode($xml);
        $data = json_decode($json, true);
        return [$data];
    }

    /** Парсим csv
     * @param       $file
     * @param array $params - delimiter, rowSeparator
     * @return array
     */
    private function parseCSV($file, $params)
    {
        $delimiter    = !empty($params['delimiter']) ? $params['delimiter'] : '|';
        $rowSeparator = !empty($params['rowSeparator']) ? $params['rowSeparator'] : "\r\n";
        $text         = file_get_contents($file);
        $stings_arr   = explode($rowSeparator, $text);
        $data         = [];
        foreach ($stings_arr as $string) {
            $data[] = explode($delimiter, $string);
        }
        return [$data];
    }

    private static function parseDOC($file, $params)
    {
    }

    private static function parseRTF($file, $params)
    {
        $text = file_get_contents($file);
        if (!strlen($text)) {
            return "";
        }

        if (strlen($text) > 1024 * 1024) {
            $text = preg_replace("#[\r\n]#", "", $text);
            $text = preg_replace("#[0-9a-f]{128,}#is", "", $text);
        }

        $text = str_replace("\\'3f", "?", $text);
        $text = str_replace("\\'3F", "?", $text);

        $document = "";
        $stack = array();
        $j = -1;
        $fonts = array();

        for ($i = 0, $len = strlen($text); $i < $len; $i++) {
            $c = $text[$i];
            switch ($c) {
                case "\\":
                    $nc = $text[$i + 1];
                    if ($nc == '\\' && self::rtfIsPlainText($stack[$j])) {
                        $document .= '\\';
                    } elseif ($nc == '~' && self::rtfIsPlainText($stack[$j])) {
                        $document .= ' ';
                    } elseif ($nc == '_' && self::rtfIsPlainText($stack[$j])) {
                        $document .= '-';
                    } elseif ($nc == '*') {
                        $stack[$j]["*"] = true;
                    } elseif ($nc == "'") {
                        $hex = substr($text, $i + 2, 2);
                        if (self::rtfIsPlainText($stack[$j])) {
                            if (!empty($stack[$j]["mac"]) || @$fonts[$stack[$j]["f"]] == 77) {
                                $document .= self::fromMacRoman(hexdec($hex));
                            } elseif (@$stack[$j]["ansicpg"] == "1251" || @$stack[$j]["lang"] == "1029") {
                                $document .= chr(hexdec($hex));
                            } else {
                                $document .= "&#" . hexdec($hex) . ";";
                            }
                        }
                        $i += 2;
                    } elseif ($nc >= 'a' && $nc <= 'z' || $nc >= 'A' && $nc <= 'Z') {
                        $word = "";
                        $param = null;
                        for ($k = $i + 1, $m = 0; $k < strlen($text); $k++, $m++) {
                            $nc = $text[$k];
                            if ($nc >= 'a' && $nc <= 'z' || $nc >= 'A' && $nc <= 'Z') {
                                if (empty($param)) {
                                    $word .= $nc;
                                } else {
                                    break;
                                }
                            } elseif ($nc >= '0' && $nc <= '9') {
                                $param .= $nc;
                            } elseif ($nc == '-') {
                                if (empty($param)) {
                                    $param .= $nc;
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        $i += $m - 1;
                        $toText = "";
                        switch (strtolower($word)) {
                            case "u":
                                $toText .= html_entity_decode("&#x".sprintf("%04x", $param).";");
                                $ucDelta = !empty($stack[$j]["uc"]) ? @$stack[$j]["uc"] : 1;
                                if ($ucDelta > 0) {
                                    $i += $ucDelta;
                                }
                                break;
                            case "par":
                            case "page":
                            case "column":
                            case "line":
                            case "lbr":
                            case "row":
                                $toText .= "\r\n".PHP_EOL;
                                break;
                            case "emspace":
                            case "enspace":
                            case "qmspace":
                                $toText .= " ";
                                break;
                            case "tab":
                                $toText .= "\t";
                                break;
                            case "chdate":
                                $toText .= date("m.d.Y");
                                break;
                            case "chdpl":
                                $toText .= date("l, j F Y");
                                break;
                            case "chdpa":
                                $toText .= date("D, j M Y");
                                break;
                            case "chtime":
                                $toText .= date("H:i:s");
                                break;
                            case "emdash":
                                $toText .= html_entity_decode("&mdash;");
                                break;
                            case "endash":
                                $toText .= html_entity_decode("&ndash;");
                                break;
                            case "bullet":
                                $toText .= html_entity_decode("&#149;");
                                break;
                            case "lquote":
                                $toText .= html_entity_decode("&lsquo;");
                                break;
                            case "rquote":
                                $toText .= html_entity_decode("&rsquo;");
                                break;
                            case "ldblquote":
                                $toText .= html_entity_decode("&laquo;");
                                break;
                            case "rdblquote":
                                $toText .= html_entity_decode("&raquo;");
                                break;
                            case "bin":
                                $i += $param;
                                break;
                            case "fcharset":
                                $fonts[@$stack[$j]["f"]] = $param;
                                break;
                            default:
                                $stack[$j][strtolower($word)] = empty($param) ? true : $param;
                                break;
                        }
                        if (self::rtfIsPlainText($stack[$j])) {
                            $document .= $toText;
                        }
                    } else {
                        $document .= " ";
                    }
                    $i++;
                    break;
                case "{":
                    if ($j == -1) {
                        $stack[++$j] = array();
                    } else {
                        array_push($stack, $stack[$j++]);
                    }
                    break;
                case "}":
                    array_pop($stack);
                    $j--;
                    break;
                case "\r\n":
                    $document .= "\n".PHP_EOL.PHP_EOL;
                    break;
                case "\0":
                case "\r":
                case "\f":
                case "\b":
                case "\t":
                    break;
                case "\n":
                    $document .= " ";
                    break;
                default:
                    if (self::rtfIsPlainText($stack[$j])) {
                        $document .= $c;
                    }
                    break;
            }
        }

        $file_data = html_entity_decode(iconv("windows-1251", "utf-8", $document), ENT_QUOTES, "UTF-8");

        $rows = [];
        $tmp_row = [];

        $str = preg_replace("/   +/", "   ", $file_data);
        $str = explode('   ', $str);

        foreach ($str as $key => $_str) {
            if ($key <= 18) {
                continue;
            }

            $tmp_row[] = trim($_str);
            if (count($tmp_row) == 8) {
                $rows[] = $tmp_row;
                $tmp_row = [];
            }
        }

        return [$rows];
    }

    private static function rtfIsPlainText($s)
    {
        $failAt = array("*", "fonttbl", "colortbl", "datastore", "themedata", "stylesheet", "info", "picw", "pich");
        for ($i = 0; $i < count($failAt); $i++) {
            if (!empty($s[$failAt[$i]])) {
                return false;
            }
        }
        return true;
    }

    private static function fromMacRoman($c)
    {
        $table = array(
            0x83 => 0x00c9, 0x84 => 0x00d1, 0x87 => 0x00e1, 0x8e => 0x00e9, 0x92 => 0x00ed,
            0x96 => 0x00f1, 0x97 => 0x00f3, 0x9c => 0x00fa, 0xe7 => 0x00c1, 0xea => 0x00cd,
            0xee => 0x00d3, 0xf2 => 0x00da
        );
        if (isset($table[$c])) {
            $c = "&#x" . sprintf("%04x", $table[$c]) . ";";
        }
        return $c;
    }
}
