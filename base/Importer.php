<?php
namespace emilasp\imports\base;

use emilasp\imports\helpers\ImportHelper;
use Yii;
use yii\base\Component;
use yii\helpers\FileHelper;

/** Базовый класс импорта (поулчения данных из БД, с почты , из файла)
 * Class Importer
 * @package emilasp\imports\base
 */
abstract class Importer extends Component
{
    /** @var  ImportConf компонент конфигурации импорта */
    public $owner;

    /** @var integer ID поставщика */
    public $sellerId;

    /** @var  array поулченные импортером данные (пачками)*/
    public $batchData = [];

    /** основной метод выполнения импорта данных */
    abstract public function process();

    /** Метод выполняется после выполнения импорта */
    abstract public function afterProcess();
}
