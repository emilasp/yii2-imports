<?php
namespace emilasp\imports\base;

use ErrorException;
use yii\base\Component;

/** Базовый класс экспорта(вставки данных в БД, отправка на почту, в файл)
 * Class Exporter
 * @package emilasp\imports\base
 */
abstract class Exporter extends Component
{
    /** @var  ImportConf компонент конфигурации импорта */
    public $owner;

    /** @var  array структура колонок для вставки  */
    protected $columnsName;

    /** Реализация метода форматирования строки перед вставкой
     * @param array $row
     * @return array
     */
    abstract public function formatRow($row);

    /** вставляем данные
     * @param $data
     */
    abstract public function insertData($data);

    public function init()
    {
        parent::init();
        if (!$this->columnsName) {
            throw new ErrorException('Для данного импорта не указаны обязательные для вставки колонки $columnsName');
        }
    }

}
