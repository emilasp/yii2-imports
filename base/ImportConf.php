<?php

namespace emilasp\imports\base;

use Yii;
use yii\base\Component;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;
use yii\log\Logger;

/**
 * Class ImportConf
 * @package emilasp\imports\base
 */
abstract class ImportConf extends Component
{
    /** Префикс для метода обработки колонки */
    const METHOD_PREFIX = 'col';

    /** @var int Начальная строка */
    public $sellerId;

    /** @var array Таксономии (для подгрузки и создания вариаций) */
    public $taxonomies = [
        /*'Размер экрана' => [
            'col' => 3,
            'typeVariation' => 'pseudo',
        ],*/
    ];

    /** @var int Начальная строка */
    public $startRow = 1;
    /** @var int Начальная колонка */
    public $startColumn = 1;

    /** @var  array|Importer */
    public $importer;
    /** @var  array|Exporter */
    public $exporter;
    /** @var */
    public $owner;
    /** @var array Логи построчно */
    public $log = [];

    /** @var bool Вызывается из консольного приложения */
    public $isConsole = true;
    /** @var bool Выводить прогрессбар */
    public $progress = false;

    /** @var  array Соответствие категорий поставщика категориям сайта */
    protected $categorySeller = [];

    /** @var array данные для добавления в каждую строку */
    protected $sharedData = [];
    /** @var array результирующие строки */
    protected $resultRows;

    /** @var array Текущая строкa */
    private $currentRow = [];
    /** @var int Текущая позиция строки */
    private $currentRowIndex = 0;
    /** @var int Текущая позиция колонки */
    private $currentColIndex = 0;

    /** @var bool Добавлять запись лога в кеш */
    private $addLogsInCache = false;

    public function init()
    {
        parent::init();

        $this->initImporter();
        $this->initExporter();
        $this->checkRequiredAttr();
    }

    /** Складываем в логи
     *
     * @param     $message
     * @param int $type
     */
    public function log($message, $type = Logger::LEVEL_INFO)
    {
        $dataRow = [];
        foreach ($this->currentRow as $index => $col) {
            $dataRow[$index] = @iconv('utf-8', 'UTF-8//IGNORE//TRANSLIT', $col);
        }
        $dataRowJson                                                                   = Json::encode($dataRow);
        $this->log[$this->currentRowIndex][$this->currentColIndex]['data']             = $dataRowJson;
        $this->log[$this->currentRowIndex][$this->currentColIndex]['message'][$type][] = $message;
        if ($type == Logger::LEVEL_ERROR) {
            Yii::error($message . '  (_|_)  ' . $dataRowJson, 'import_' . $this->sellerId);
        }
    }

    /**
     * Получаем кеш логов по текущей сессии
     */
    public static function getCacheLog()
    {
        $logs = Yii::$app->cache->get(Yii::$app->user->id . 'LogImport');
        return $logs;
    }

    /** Разбираем файлы
     *
     * @param bool|false $addLogsInCache
     */
    public function process($addLogsInCache = false)
    {
        $this->addLogsInCache = $addLogsInCache;

        $this->importer->process();

        foreach ($this->importer->batchData as $index => $batch) {
            $rows             = $this->preRows($batch);
            $this->sharedData = $this->getSharedData($rows);
            $this->resultRows = $this->normalizeRows($rows);
            $this->resultRows = $this->collectTaxonomies();
            $this->exporter->insertData($this->resultRows);
        }

        $this->importer->afterProcess();
    }

    /** Объединяем строки с группировкой таксономий и установки типа товара
     * @return mixed
     */
    protected function collectTaxonomies()
    {
        $result    = [];
        $countRows = count($this->resultRows);
        $this->startProgressBar('Собираем таксономии', $countRows);

        foreach ($this->resultRows as $index => $row) {
            if (!isset($result[$row['article']])) {
                $result[$row['article']] = $row;
            }

            foreach ($row['taxonomies'] as $colName => $col) {
                if (!isset($this->taxonomies[$colName]) || $col === '') {
                    continue;
                }

                // устанавливаем тип вариации товара в зависимости от старшего типа таксономии(свойства)
                $taxonomy = $this->taxonomies[$colName];

                if (!isset($result[$row['article']]['type'])) {
                    $result[$row['article']]['type'] = $row['type'];
                }

                if ($taxonomy['type'] == 3
                    && (isset($result[$row['article']]['type']) && $result[$row['article']]['type'] !== 2)
                ) {
                    $result[$row['article']]['type'] = 3;
                } elseif ($taxonomy['type'] == 2) {
                    $result[$row['article']]['type'] = 2;
                } else {
                    $result[$row['article']]['type'] = 1;
                }

                // проверяем что таких значений для таксономии нет
                $find = array_search($col, $result[$row['article']]['taxonomies']);
                if ($find === false) {
                    $result[$row['article']]['taxonomies'][$colName][] = $col;
                }
            }
            $this->processProgressBar($index, $countRows);
        }
        $this->endProgressBar();
        return $result;
    }

    /** Получаем строки
     *
     * @param $batch
     *
     * @return array
     */
    protected function preRows($batch)
    {
        return $batch;
    }

    /** Получаем данные, общие для всех строк
     *
     * @param $rows
     *
     * @return array
     */
    protected function getSharedData($rows)
    {
        return $this->sharedData;
    }

    /** Обрабатываем строки
     *
     * @param $rows
     *
     * @return mixed
     */
    protected function normalizeRows($rows)
    {
        $countRows = count($rows);
        $this->startProgressBar('Нормализуем строки', $countRows);

        $resultRows = [];
        foreach ($rows as $index => $row) {
            $this->processProgressBar($index, $countRows);

            if ($index < $this->startRow) {
                continue;
            }
            $this->currentRow      = $row;
            $this->currentRowIndex = $index;
            $row                   = $this->normalizeCols($row);

            if ($afterFormat = $this->exporter->formatRow($row)) {
                $resultRows[] = $afterFormat;
                $this->log('Удачно обработана строка', Logger::LEVEL_TRACE);
            }
        }

        $this->endProgressBar();

        return $resultRows;
    }

    /** Обрабатываем колонки
     *
     * @param $row
     *
     * @return mixed
     */
    protected function normalizeCols($row)
    {
        $rowResult = [];
        foreach ($row as $index => $col) {
            $nameFunc = $index;
            if (is_numeric($index)) {
                $nameFunc = $index + 1;
            }
            $method = self::METHOD_PREFIX . $nameFunc;

            if (method_exists(static::className(), $method)) {
                $this->currentColIndex = $nameFunc;
                $rowResult             = ArrayHelper::merge($rowResult, static::$method($col), $this->sharedData);
            }
        }
        /** Получаем изображения */
        /*$rowResult = ArrayHelper::merge($rowResult, $this->getImages($row));
        $rowResult = ArrayHelper::merge($rowResult, $this->getCategories($row));*/

        return $rowResult;
    }

    /** Проверяем обязательные свойства объекта конфигурации
     * @throws ErrorException
     */
    private function checkRequiredAttr()
    {
        if (!$this->sellerId) {
            throw new ErrorException('Свойство sellerId обязательно должно быть заполнено в классе конфигурации');
        }
    }

    /**
     * Инициализаруем Импортер
     */
    private function initImporter()
    {
        $this->importer['sellerId'] = $this->sellerId;
        $this->importer             = Yii::createObject($this->importer);
        $this->importer->owner      = $this;
    }

    /**
     * Инициализируем экспортер
     */
    private function initExporter()
    {
        $this->exporter['sellerId'] = $this->sellerId;
        $this->exporter             = Yii::createObject($this->exporter);
        $this->exporter->owner      = $this;
    }

    /** Старутем прогресс бар для консоли
     *
     * @param $rows
     * @param $title
     */
    private function startProgressBar($title, $countRows)
    {
        if ($this->isConsole) {
            Yii::$app->controller->message("{$title}({$countRows}).\n", Console::FG_GREEN);
            if ($this->progress) {
                Console::startProgress(0, $countRows, 'Прогресс: ');
            }
        }
    }

    /** Итерация прогресс бара для консоли
     *
     * @param $index
     * @param $countRows
     */
    private function processProgressBar($index, $countRows)
    {
        if ($this->isConsole && $this->progress) {
            Console::updateProgress($index + 1, $countRows);
        }
    }

    /** Завершаем прогресс бар для консоли
     */
    private function endProgressBar()
    {
        if ($this->isConsole && $this->progress) {
            Console::endProgress();
        }
    }
}
