<?php
namespace emilasp\imports\imports;

use emilasp\imports\base\ImportConf;
use DateTime;
use emilasp\taxonomy\models\Category;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\log\Logger;

/**
 * Class TestConf
 * @package emilasp\imports\imports
 */
class TestConf extends ImportConf
{
    public $importer = [
        'class' => 'emilasp\imports\providers\importers\FileImporter',
    ];

    public $exporter = [
        'class' => 'emilasp\imports\providers\exporters\ProductTableExporter',
    ];

    public $sellerId = 1;

    public $startRow = 8;

    /** @var array Таксономии (для подгрузки и создания вариаций) */
    public $taxonomies = [
        'Размер'    => [
            'col'  => 3,
            'type' => 3,
        ],
        'Состав'    => [
            'col'  => 4,
            'type' => 1,
        ],
        'Цвет'      => [
            'col'  => 10,
            'type' => 3,
        ],
    ];

    /** Артикул */
    public function colArticul($col)
    {
        return ['article' => $col];
    }

    /** Категории */
    public function colCategory($col)
    {
        $cats = [];
        $categories = explode(',', $col);
        foreach ($categories as $sellerCat) {
            if (isset($this->categorySeller[$sellerCat])) {
                $cats[] = $this->categorySeller[$sellerCat];
            }
        }

        if (!count($cats)) {
            $cats[] = 1;
        }

        return ['categories' => $cats];
    }

    /** Пол */
    public function colSex($col)
    {
        $sex = null;
        switch ($col) {
            case 'male':
                $sex = 1;
                break;
            case 'Женский':
                $sex = 2;
                break;
            case 'unisex':
                $sex = 3;
                break;
        }
        return ['sex' => $sex];
    }

    /** Таксономия: Коллекция */
    public function colYear($col)
    {
        return ['year' => $col];
    }


    /** Наименование */
    public function colTitle($col)
    {
        return ['name' => $col];
    }

    /** Цена розн. */
    public function colPrice($col)
    {
        return ['cost' => $col];
    }

    /** Таксономия: Размер */
    public function colCounts($col)
    {
        $categories = Json::decode($col);
        return ['taxonomies' => ['Размер' => array_keys($categories)]];
    }

    /** Таксономия: Цвет */
    public function colColor($col)
    {
        return ['taxonomies' => ['Цвет' => explode('/', $col)]];
    }

    /** Таксономия: Состав */
    public function colComposition($col)
    {
        return ['taxonomies' => ['Состав' => $col]];
    }

    /** Старна */
    public function colManufacturer($col)
    {
        return ['country' => $col];
    }


    /** Описание */
    public function colDescription($col)
    {
        if (!is_string($col)) {
            $col = '';
        }
        return ['text' => $col];
    }

    /** Короткое описание */
   /* public function colShort_description($col)
    {
        var_dump($col);
        return ['short_text' => (string)$col];
    }*/


    /** Изображение */
    public function colImage($col)
    {

        return ['images' => [$col['@attributes']['url']]];
    }

    /** Изображение */
    public function colGallery($col)
    {
        if (!isset($col['@attributes']['url'])) {
            return ['images' => []];
        }
        return ['images' => [$col['@attributes']['url']]];
    }

    /** Получаем строки
     *
     * @param $batch
     *
     * @return array
     */
    protected function preRows($batch)
    {
        $categories = $batch['categories']['item'];
        $this->processCategories($categories);
        $rows = $batch['products']['item'];

        $result = [];
        foreach ($rows as $row) {
            $data = $row['@attributes'];
            unset($row['@attributes']);
            $result[] = ArrayHelper::merge($row, $data);
        }

        return $result;
    }

    /** Получаем строки
     *
     * @param $batch
     *
     * @return array
     */
    protected function processCategories($categories)
    {
        /*$root = Category::findOne(1);
        $cats = [];
        foreach ($categories as $category) {
            $category = $category["@attributes"];
            $cat      = Category::findOne(['id' => $this->categorySeller[$category['id']]]);

            if (!$cat) {
                $cat             = new Category();
                $cat->name       = $category['name'];
                $cat->text       = $category['name'];
                $cat->short_text = $category['name'];
                $cat->type       = 1;
                $cat->status     = 2;

                if (!$category['parentid'] || !isset($cats[$category['parentid']])) {
                    $cat->appendTo($root);
                } else {
                    $cat->appendTo($cats[$category['parentid']]);
                }

            }
            $cats[$category['id']] = $cat;
        }*/
        return $categories;
    }

    protected $categorySeller = [
        0  => 2,
        1  => 2,
        2  => 3,
        3  => 4,
        4  => 5,
        5  => 6,
        6  => 7,
        7  => 8,
        8  => 2,
        9  => 4,
        20 => 2,
        21 => 2,
        22 => 3,
        23 => 4,
        24 => 5,
        25 => 6,
        26 => 7,
        27 => 8,
        28 => 2,
        29 => 4,
        30 => 2,
        31 => 2,
        32 => 3,
        33 => 4,
        34 => 5,
        35 => 6,
        36 => 7,
        37 => 8,
        38 => 2,
        39 => 4,
        40 => 2,
        41 => 2,
        42 => 3,
        43 => 4,
        44 => 5,
        45 => 6,
        46 => 7,
        47 => 8,
        48 => 2,
        49 => 4,
        50 => 2,
        51 => 2,
        52 => 3,
        53 => 4,
        54 => 5,
        55 => 6,
        56 => 7,
        57 => 8,
        58 => 2,
        59 => 4,
        60 => 2,
        61 => 2,
        62 => 3,
        63 => 4,
        64 => 5,
        65 => 6,
        66 => 7,
        67 => 8,
        68 => 2,
        69 => 4,
        70 => 2,
        71 => 2,
        72 => 3,
        73 => 4,
        74 => 5,
        75 => 6,
        76 => 7,
        77 => 8,
        78 => 2,
        79 => 4,
        80 => 2,
        81 => 2,
        82 => 3,
        83 => 4,
        84 => 5,
        85 => 6,
        86 => 7,
        87 => 8,
        88 => 2,
        89 => 4,
    ];
}
