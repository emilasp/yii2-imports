<?php
namespace emilasp\imports;

use emilasp\imports\controllers\ImportController;
use emilasp\core\CoreModule;
use Yii;
use yii\base\BootstrapInterface;
use yii\console\Application;
use yii\helpers\FileHelper;

/**
 * Class ImportsModule
 * i */
class ImportsModule extends CoreModule implements BootstrapInterface
{
    /** @var string */
    public $id        = 'imports';
    public $pathCache = '@console/runtime/import/cache';
    public $pathIn    = '@console/runtime/import/parse/in';
    public $pathOut   = '@console/runtime/import/parse/out';

    public $imports = [];

    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $app->controllerMap[$this->id] = [
                'class'  => ImportController::className(),
                'module' => $this,
            ];
        }
    }

    public function init()
    {
        parent::init();

        $this->pathCache = Yii::getAlias($this->pathCache);
        $this->pathIn    = Yii::getAlias($this->pathIn);
        $this->pathOut   = Yii::getAlias($this->pathOut);

        $this->checkAndCreatePaths();
    }

    /**
     * Создаём базовые директории для работы модуля
     */
    private function checkAndCreatePaths()
    {
        if (!is_dir($this->pathCache)) {
            FileHelper::createDirectory($this->pathCache);
        }
        if (!is_dir($this->pathIn)) {
            FileHelper::createDirectory($this->pathIn);
        }
        if (!is_dir($this->pathOut)) {
            FileHelper::createDirectory($this->pathOut);
        }
    }
}
